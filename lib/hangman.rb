class Hangman
  attr_reader :guesser, :referee, :board, :guess, :guesses

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
  end

  def setup
    word_length = @referee.pick_secret_word
    @guesser.register_secret_length(word_length)
    @board = Array.new(word_length, "_")
    @guesses = []
  end

  def take_turn
    guesser_guess = @guesser.guess(@board)
    correct_idx = @referee.check_guess(guesser_guess)
    update_board(correct_idx, guesser_guess)
    @guesses << guesser_guess
    @guesser.handle_response(guesser_guess, correct_idx)
  end

  def update_board(correct_idx, guess)
    correct_idx.each { |idx| @board[idx] = guess }
  end

  def display_board
    p @board
  end

  def play
    puts "How many guesses will you allow?"
    guess_number = gets.chomp.to_i
    setup
    i = 0
    until i == guess_number || won?
      take_turn
      display_board
      puts "You have guessed #{@guesses} so far."
      i += 1
    end
    if !won?
      return "Guesser lost! The word was #{@referee.secret_word.upcase}"
    else
      return "Good job! You correctly guessed #{@referee.secret_word.upcase}!"
    end
  end

  def won?
    return false if @board.include?("_")
    true
  end

end

class HumanPlayer

  attr_accessor :guess, :secret_word


  def initialize(secret_word)
    @secret_word = secret_word
  end

  def guess(board)
    puts "Which letter would you like to guess?"
    @guess = gets.chomp
  end

  def check_guess(letter_str)
    result = []
    @secret_word.split("").each_with_index do |char, idx|
      result << idx if char == letter_str
    end
    result
  end

  def pick_secret_word
    @secret_word.length
  end

  def register_secret_length(length)
    puts "There are #{length} letters in this word."
  end

  def handle_response(guess, position)
    if position.empty?
      puts "Sorry, there is no #{guess} in this word."
    else
      puts "There is #{guess} at #{position}"
    end
  end

end

class ComputerPlayer

  attr_accessor :dictionary, :secret_word, :secret_length, :guess


  def initialize(dictionary=File.readlines("dictionary.txt"))
    @dictionary = dictionary.map { |word| word.chomp }
  end

  def candidate_words
    @dictionary#.select! {|word| word.length == @secret_length}
  end

  #def dictionary_text
    #@dictionary_text = File.readlines("dictionary.txt")
    #@dictionary_text.map! { |word| word.chomp }
  #end

  def pick_secret_word
    @secret_word = @dictionary[rand(0...@dictionary.length)]
    @secret_word.length
  end

  def register_secret_length(length)
    @secret_length = length
    @dictionary.select! {|word| word.length == length}
  end

  def guess(board)
    #if board.all? {|item| item.nil?} == false
      #update_letters = board.reject { |letter| letter.nil? }.uniq
      #update_letters.each do |letter|
      #  result = []
      #  board.each_with_index do |char, idx|
    #      result << idx if char == letter
  #      end
  #      handle_response(letter, result)
  #    end
  #  end
    letters = Hash.new(0)
    @dictionary.each do |word|
      word.split("").each_with_index do |letter|
        letters[letter] += 1 if !board.include?(letter)
      end
    end
    letters.sort_by {|k, v| v}.last.first

  end

  def check_guess(letter_str)
    result = []
    @secret_word.split("").each_with_index do |char, idx|
      result << idx if char == letter_str
    end
    result
  end

  def handle_response(guess, position)
    if position.empty?
      @dictionary.reject! {|word| word.include?(guess)}
    else
      deletes = []
      @dictionary.each do |word|
        places = []
        word.split("").each_with_index do |letter, idx|
          places << idx if letter == guess
        end
        deletes << word if position != places
      end
      deletes.each { |word| @dictionary.delete(word) }
    end
  end

end
